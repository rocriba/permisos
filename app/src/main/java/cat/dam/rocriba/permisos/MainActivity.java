package cat.dam.rocriba.permisos;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity {
    //Inicialitzem Constants
    final int MY_PERMISSIONS_REQUEST_CAMERA=1;
    final int MY_PERMISSIONS_REQUEST_STORAGE=1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Inizialitzem variables
        Button btn_1 = (Button) findViewById(R.id.btn_1);
        Button btn_2 = (Button) findViewById(R.id.btn_2);

        btn_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Comprovem permís, si el té mostrem el toast, sinó el demanem
                if (ContextCompat.checkSelfPermission(MainActivity.this,
                        Manifest.permission.CAMERA)
                    !=PackageManager.PERMISSION_GRANTED){

                    //NO esta otrogat, demanem el permís

                    ActivityCompat.requestPermissions(MainActivity.this,
                            new String[]{Manifest.permission.CAMERA},
                            MY_PERMISSIONS_REQUEST_CAMERA);
                } else {
                    Toast.makeText(getApplicationContext(), "El permís de càmera ja està concedit", Toast.LENGTH_SHORT).show();
                }
            }
        });



        //En cas de clicar Boto 2

        btn_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                  //Comprovem permís, si el té mostrem el toast, sinó el demanem. Demanem els dos permísos al'hora
                if (ContextCompat.checkSelfPermission(MainActivity.this,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        !=PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(MainActivity.this,
                        Manifest.permission.READ_EXTERNAL_STORAGE)
                        !=PackageManager.PERMISSION_GRANTED){

                    //Demanem els dos permisos per separat
                    ActivityCompat.requestPermissions(MainActivity.this,
                            new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                            MY_PERMISSIONS_REQUEST_STORAGE);

                    ActivityCompat.requestPermissions(MainActivity.this,
                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            MY_PERMISSIONS_REQUEST_STORAGE);

                } else {
                    //En cas de ja està otorgat mostrem el toast
                    Toast.makeText(getApplicationContext(), "El permís de emmagatzematge ja està concedit", Toast.LENGTH_SHORT).show();

                }
            }
        });
    }
}